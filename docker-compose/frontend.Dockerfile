

FROM httpd:2.4-alpine

RUN apk update; 
   
RUN apk add apache2 curl 
#RUN apt-get update && apt-get install -y --no-install-recommends apache2
COPY httpd.conf /usr/local/apache2/conf/httpd.conf
#COPY vhosts/backend.vhost.conf /etc/httpd/sites-available/
#COPY index.html /var/www/html/
EXPOSE 80
ADD vhosts/backend.vhost.conf /usr/local/apache2/conf/vhosts/backend.vhost.conf
#RUN ln -s /etc/httpd/sites-available/backend.vhost.conf /etc/httpd/sites-enabled/backend.vhost.conf
#ADD httpd.conf /etc/httpd/conf/




#FROM httpd:latest


#COPY httpd.conf /usr/local/apache2/conf/httpd.conf


# You can use the image to create N number of different virtual hosts
#RUN mkdir -p /usr/local/apache2/conf/sites/

# To tell docker to expose this port
#EXPOSE 80


#CMD ["httpd", "-D", "FOREGROUND"]
