#!/bin/bash

docker build -t my-test2.6 --build-arg  ANSIBLE_VERSION=2.6.6 .  
docker build -t my-test2.7 --build-arg  ANSIBLE_VERSION=2.7.7 .

docker run my-test2.6 && docker run my-test2.7

